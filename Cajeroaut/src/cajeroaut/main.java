package cajeroaut;

import java.util.Scanner;

public class main {
    public static Scanner s = new Scanner(System.in);
    public static int saldoInicial, conteoServicios, precioTiempoAire, precioLuz, precioAgua;

    public static void main(String[] args) {
        String usuario, contraseña;

        System.out.println("teclee usuario:");
        usuario = s.nextLine().toLowerCase();
        System.out.println("teclee contraseÃ±a:");
        contraseña = s.nextLine().toLowerCase();

        Usuario sesion = new Usuario(usuario, contraseña);
        saldoInicial = sesion.saldo;

        listarServicios();
        cajero(sesion);
        System.out.println("Saldo inicial: $"+saldoInicial);
        System.out.println("Numero de servicios contratados: "+conteoServicios);
        System.out.println("Saldo final: $"+sesion.saldo);

    }

    public static void cajero(Usuario sesion) {
        boolean contratar = true;

        do {
            System.out.println("tecleea contratar, cancelar o retirar:");
            String accion = s.nextLine();

            switch (accion){
                case "contratar":
                    contratarServicios(sesion);
                    break;
                case "cancelar":
                    cancelarServicios(sesion);
                    break;
                case "retirar":
                    retirarEfectivo(sesion);
                    break;
            }

            System.out.println("teclea exit para salir, o cualquier cosa para seguir:");
            String salir = s.nextLine();
            if (salir.equalsIgnoreCase("exit")) {
                contratar = false;
            } else {
                contratar = true;
            }
        }while (contratar);
    }

    public static void listarServicios(){
        System.out.println("Servicios disponibles:");
        System.out.println("Servicio - Precios");
        System.out.println("Tiempo aire: $50 - $100 - $200");
        System.out.println("Luz: $250");
        System.out.println("Agua $100");
    }

    public static void contratarServicios(Usuario sesion){
        Scanner saux = new Scanner(System.in);

        System.out.println("teclee el servicio que desea contratar, perro:");
        String servicio = saux.nextLine().toLowerCase();
        System.out.println("procesando...");

        switch (servicio){
            case "tiempo aire":
                System.out.println("seleccione el monto de tiempo aire a elegir entre 50, 100 y 200");
                int monto = saux.nextInt();
                switch (monto){
                    case 50:
                    case 200:
                    case 100:
                        sesion.saldo = sesion.saldo - monto;
                        conteoServicios ++;
                        System.out.println("servicio contratado exitosamente");
                        break;
                    default:
                        System.out.println("monto no valido");
                        break;
                }
                break;
            case "luz":
                sesion.saldo = sesion.saldo - 250;
                conteoServicios ++;
                System.out.println("servicio contratado exitosamente");
                break;
            case "agua":
                sesion.saldo = sesion.saldo - 100;
                conteoServicios ++;
                System.out.println("servicio contratado exitosamente");
                break;
        }
    }

    public static void cancelarServicios(Usuario sesion){
        Scanner saux = new Scanner(System.in);

        System.out.println("teclee el servicio que desea cancelar, perro:");
        String servicio = saux.nextLine().toLowerCase();
        System.out.println("procesando...");

        switch (servicio){
            case "tiempo aire":
                System.out.println("seleccione el monto de tiempo aire a elegir entre 50, 100 y 200");
                int monto = saux.nextInt();
                switch (monto){
                    case 50:
                    case 200:
                    case 100:
                        sesion.saldo = sesion.saldo + monto;
                        conteoServicios --;
                        System.out.println("servicio descontratado exitosamente");
                        break;
                    default:
                        System.out.println("monto no valido");
                        break;
                }
                break;
            case "luz":
                sesion.saldo = sesion.saldo + 250;
                conteoServicios --;
                System.out.println("servicio descontratado exitosamente");
                break;
            case "agua":
                sesion.saldo = sesion.saldo + 100;
                conteoServicios --;
                System.out.println("servicio descontratado exitosamente");
                break;
        }
    }

    public static void retirarEfectivo(Usuario sesion) {
        Scanner saux = new Scanner(System.in);
        int monto;

        System.out.println("Digite monto a retirar:");
        monto = s.nextInt();
        if (sesion.saldo < monto){
            System.out.println("Saldos insuficientes, perro vagabundo");
        }
        if ((monto % 50) == 0) {
            sesion.saldo = sesion.saldo - monto;
            System.out.println("Has retirado: $"+monto);
            System.out.println("saldo actual: $"+sesion.saldo);
        } else {
            System.out.println("Solo se permiten multiplos de 50 pesitos :'v");
        }
    }
}
